import EventEmitter from "eventemitter3";
import anime from "animejs";

export default class Application extends EventEmitter {
  static get events() {
    return {
      READY: "ready",
    };
  }

  constructor() {
    super();
    this.init();
    this.emit(Application.events.READY);
  }

  init() {
    let article = document.querySelector("article");
    
    article.addEventListener("click", () => {
      anime({
        targets: article,
        translateX: 200,
        direction: 'alternate',
        easing: 'spring(1, 100, 10, 0)',
      });
    });
  }
}
